var addresses;

function deleteAddressRow(e) {
    var searchStr = document.getElementById("SearchData").value.toLowerCase();
    var id = e.target.id;
    addresses.splice(id, 1);
    localStorage.setItem("Addresses", JSON.stringify(addresses));
    populateTable(searchStr);
};

function insertAddressRow(data, id) {
    var table = document.getElementById("AddressTable").getElementsByTagName("tbody")[0];
    var delBtn = document.createElement("button");
    delBtn.className = "deleteButton";
    delBtn.innerHTML = "X";
    delBtn.id = id;
    delBtn.addEventListener("click", deleteAddressRow);

    var row = table.insertRow();
    row.insertCell(0).innerHTML = data[0].value + " " + data[1].value;
    row.insertCell(1).innerHTML = data[2].value;
    row.insertCell(2).innerHTML = data[3].value;
    row.insertCell(3).innerHTML = data[4].value;
    row.insertCell(4).innerHTML = data[5].value;
    row.insertCell(5).innerHTML = data[6].value;
    row.insertCell(6).innerHTML = data[7].value;
    row.insertCell(7).appendChild(delBtn);
};


function populateTable(filter) {
    if (typeof (filter) === "undefined")
        filter = "";
    
    document.getElementById("AddressTable").getElementsByTagName("tbody")[0].innerHTML = "";
    for (var i = 0; i < addresses.length; i++) {
        if (addresses[i][0].value.toLowerCase().includes(filter)) {
            insertAddressRow(addresses[i], i);
        }
    }
};

function getCountries() {
    $.ajax({
        type: 'GET',
        url: 'Home/getCountries',
        dataType: 'json',
        success: function (data) {
            $("#Country").autocomplete({
                lookup: data
            });
        }
    });
};


$("#ClearButton").click(function (e) {
    document.getElementById("AddressForm").reset();
    $("#Submit").attr("disabled", "disabled");
    return false;
});

$("#AddressForm").submit(function (e) {
    var data = $("#AddressForm").serializeArray();
    addresses.push(data);
    localStorage.setItem("Addresses", JSON.stringify(addresses));
    populateTable();
    return false;
});

$("#SearchForm").submit(function (e) {
    var searchStr = document.getElementById("SearchData").value.toLowerCase();
    populateTable(searchStr);
    return false;
});

$("#AddressForm > input").keyup(function () {
    var empty = false;
    $("#AddressForm > input").each(function () {
        if ($(this).val() == "") {
            empty = true;
        }
    });
    if (empty) {
        $("#Submit").attr("disabled", "disabled");
    }
    else {
        $("#Submit").removeAttr("disabled");
    }
}); 


addresses = JSON.parse(localStorage.getItem("Addresses"));

if (addresses === null)
    addresses = new Array();

getCountries();
populateTable();



