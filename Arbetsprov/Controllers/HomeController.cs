﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Arbetsprov.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCountries()
        {
            string[] countries = {"Sweden", "Norway", "Denmark", "Finland", "Germany" };


            return Json(countries, JsonRequestBehavior.AllowGet);
        }

    }
}
